﻿using Android.App;
using Android.Content;
using Android.OS;

namespace GramChain.Test.AndroidApp
{
	[Activity(Label = "CallbackInterceptorActivity")]
	[IntentFilter(
		new[] {Intent.ActionView},
		Categories = new[] {Intent.CategoryDefault, Intent.CategoryBrowsable},
		DataScheme = "net.gramchain.test-oauth-api.scanner",
		DataHost = "callback")]
	public class CallbackInterceptorActivity : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			Finish();

			// get URI, send with mediator
			AndroidClientChromeCustomTabsApplication.Mediator.Send(this.Intent.DataString);

			StartActivity(typeof(MainActivity));
		}
	}
}