﻿using System;

namespace GramChain.Test.AndroidApp {
	/// <summary>
	///     A spot price as recorded on GramChain
	/// </summary>
	public class GramChainSpotPrice
	{
		public DateTime SpotUtc { get; set; }
		public string MetalCode { get; set; }
		public decimal Bid { get; set; }
		public decimal Ask { get; set; }
		public decimal Mid { get; set; }
	}
}