﻿using System.Collections.Generic;

namespace GramChain.Test.AndroidApp {
	/// <summary>
	///     Actual data type returned by /api/Secured/GetSpotPrices
	/// </summary>
	public class GramChainSpotPriceWrapper
	{
		public List<GramChainSpotPrice> Table { get; set; }
	}
}