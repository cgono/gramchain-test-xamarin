﻿using System;
using System.Net;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Widget;
using IdentityModel.OidcClient;
using RestSharp;
using RestSharp.Authenticators;
using Serilog;
using Xamarin.Essentials;

namespace GramChain.Test.AndroidApp
{
	[Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
	public class MainActivity : AppCompatActivity
	{
		private OidcClient _oidcClient;
		private ILogger _logger;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			Platform.Init(this, savedInstanceState);
			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.activity_main);
			WireEvents();

			this._logger = new LoggerConfiguration()
				.MinimumLevel.Verbose()
				.Enrich.FromLogContext()
				.WriteTo.AndroidLog(
					outputTemplate:
					"[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message}{NewLine}{Exception}{NewLine}")
				.CreateLogger();

#if DEBUG
			// Ignore SSL checks
			ServicePointManager.ServerCertificateValidationCallback =
				(sender, certificate, chain, errors) => true;
#endif
		}

		private void WireEvents()
		{
			var loginButton = FindViewById<Button>(Resource.Id.loginButton);
			var retrieveButton = FindViewById<Button>(Resource.Id.retrieveButton);

			loginButton.Click += Login;
			retrieveButton.Click += GetSpotPrices;
		}

		public override void OnRequestPermissionsResult(int requestCode, string[] permissions,
		                                                [GeneratedEnum] Permission[] grantResults)
		{
			Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

			base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
		}


		private async void Login(object sender, EventArgs e)
		{
			var opts = new OidcClientOptions
			           {
				           Authority = "https://test-oauth-api.gramchain.net/identity",
				           ClientId = "scanner:00:90:4C:07:71:12",
				           ClientSecret = "7faf9e34-906b-4f71-8f41-adc66d0dca67",
				           Scope = "openid profile offline_access read write-event",
				           RedirectUri = "net.gramchain.test-oauth-api.scanner://callback",
				           ResponseMode = OidcClientOptions.AuthorizeResponseMode.Redirect,
				           Browser = new ChromeCustomTabsWebView(this),
						   LoadProfile = true, 
						   
			           };

			this._oidcClient = new OidcClient(opts);
			var authResult = await this._oidcClient.LoginAsync();
			var textOutputBox = FindViewById<AppCompatTextView>(Resource.Id.Outputs);
			if (!string.IsNullOrWhiteSpace(authResult.Error))
			{
				this._logger.Error("Error: {authResult}", authResult);
				textOutputBox.Text = authResult.Error;
				return;
			}
			Settings.AuthToken = authResult.AccessToken;
		}

		private async void GetSpotPrices(object sender, EventArgs e)
		{
			var restClient = new RestClient("https://test-oauth-api.gramchain.net/API/")
			{
				Authenticator = new JwtAuthenticator(Settings.AuthToken)
			};
			var req = new RestRequest("Secured/GetSpotPrices", Method.POST, DataFormat.None);
			req.AddParameter("CallerID", "net.gramchain.test-oauth-api.scanner")
			   .AddParameter("LanguageID", "en")
			   .AddParameter("EntityID", "CACH")
			   .AddParameter("AsOfDAte", DateTime.UtcNow)
			   .AddParameter("MetalCode", "")
			   .AddParameter("ByGram", true);

			var resp = await restClient.ExecutePostTaskAsync(req);
			var textOutputBox = FindViewById<AppCompatTextView>(Resource.Id.Outputs);
			textOutputBox.Text = resp.Headers + "\n\n" + resp.Content;
		}
	}
}